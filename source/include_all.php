<?php
// SPDX-FileCopyrightText: 2024 Blender Foundation
//
// SPDX-License-Identifier: MIT-0

require_once 'request.php';
require_once 'redirector.php';
require_once 'util.php';

// NOTE: Keep bug_report.php prior to the maniphest_submit.php, as the latter
// delegates handling to the former.
require_once 'redirectors/bug_report.php';
require_once 'redirectors/maniphest_submit.php';

require_once 'redirectors/maniphest_task.php';

?>
