<?php
// SPDX-FileCopyrightText: 2024 Blender Foundation
//
// SPDX-License-Identifier: MIT-0

$DEBUG_ = false;
if (getenv('REDIRECT_DEBUG')) {
  $DEBUG_ = true;
}
if (isset($_GET['REDIRECT_DEBUG'])) {
  $DEBUG_ = true;
}

function StartsWith(string $haystack, string $needle): bool {
    return strncmp($haystack, $needle, strlen($needle)) == 0;
}

function RemovePrefix(string $str, string $prefix): string {
  if (substr($str, 0, strlen($prefix)) == $prefix) {
    return substr($str, strlen($prefix));
  }
  return $str;
}

function SplitLines(string $str): array {
  return preg_split("/((\r?\n)|(\r\n?))/", $str);
}

// Substitute variables in the given template.
// The variables are placed between double percent symbol, for example,
// %%VARIABLE%%.
function SubstituteTemplate(string $template, array $values): string {
  $result = $template;

  foreach ($values as $key => $value) {
    $result = str_replace("%%$key%%", $value, $result);
  }

  return $result;
}

// Append query parameter to an URL string.
function AppendQueryParameter(string $url, string $name, string $value): string {
  if (strpos($url, '?') !== false) {
    return $url . '&' . urlencode($name) . '=' . urlencode($value);
  }
  return $url . '?' . urlencode($name) . '=' . urlencode($value);
}

function DebugPrint(string $message) {
  global $DEBUG_;
  if (!$DEBUG_) {
    return;
  }

  print('[DEBUG] ' . htmlspecialchars($message) . "\n");
}

?>
