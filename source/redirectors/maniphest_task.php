<?php
// SPDX-FileCopyrightText: 2024 Blender Foundation
//
// SPDX-License-Identifier: MIT-0

// Redirector for legacy maniphest tasks to Gitea.

class ManiphestTaskRedirector extends Redirector {
  public function Poll(Request $request): bool {
    DebugPrint('ManiphestTaskRedirector::Poll');
    return $request->Get('__path__') == '/' &&
           $request->Get('type') == 'maniphest_task';
  }

  public function GetRedirectURL(Request $request): string {
    DebugPrint('ManiphestTaskRedirector::GetRedirectURL');

    $dir = dirname(__FILE__);

    $tasks_to_repo_json = file_get_contents(
      "$dir/maniphest_task_to_org_repos.json");
    $tasks_to_repo = json_decode($tasks_to_repo_json, true)['tasks_to_repo'];

    $id = intval($request->Get('id'));

    if ($id && array_key_exists($id, $tasks_to_repo)) {
      $repo = $tasks_to_repo[$id];
      DebugPrint("Org/Repo: $repo");
      $url = "https://projects.blender.org/{$repo}/issues/{$id}";
      return Redirector::FinalizeURL($request, $url);
    }

    DebugPrint('Task has no mapping to org/repo');
    return Redirector::FinalizeURL($request, 'https://projects.blender.org/');
  }
};

Redirector::Register(new ManiphestTaskRedirector());

?>
