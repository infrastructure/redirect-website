<?php
// SPDX-FileCopyrightText: 2024 Blender Foundation
//
// SPDX-License-Identifier: MIT-0

// Redirector from URLs used by bug report submission to Phabricator system.
// It is a compatibility layer, which deals with URLs in a format used by an
// older Blender versions. On the user level it behaves like an adapter from
// old format to new format.

class ManiphestReportInfo {
  // Operating system at which bug happens.
  public $os = '';

  // GPU used by Blender for display.
  // This is a display GPU.
  public $gpu = '';

  // String representation of Blender version with all possible details (commit
  // hash etc) against which the report is submitted.
  public $broken_version = '';

  // For add-ons: add-on name and author.
  public $addon_name = '';
  public $addon_author = '';
};

class ManiphestSubmitRedirector extends Redirector {
  private const OS_PREFIX  = "Operating system: ";
  private const GPU_PREFIX = "Graphics card: ";
  private const BROKEN_PREFIX = "Broken: ";

  private const ADDON_NAME_PREFIX = "Name: ";
  private const ADDON_AUTHOR_PREFIX = "Author: ";

  public function Poll(Request $request): bool {
    DebugPrint("ManiphestSubmitRedirector::Poll");
    return $request->Get('__path__') == '/' &&
           $request->Get('type') == 'maniphest_form';
  }

  public function GetRedirectURL(Request $request): string {
    DebugPrint("ManiphestSubmitRedirector::GetRedirectURL");

    $form_id = $request->Get('id');
    DebugPrint("Form name: $form_id");

    $info = $this->ParseDescription($request->Get('description'));

    $bug_report_info = new BugReportInfo();

    switch ($form_id) {
      case 1:
        $bug_report_info->project = BugReportInfo::PROJECT_BLENDER;
        break;
      case 2:
        $bug_report_info->project = BugReportInfo::PROJECT_BLENDER_ADDONS;
        break;
      default:
        return 'https://developer.blender.org/';
    }

    $bug_report_info->os = $info->os;
    $bug_report_info->gpu = $info->gpu;
    $bug_report_info->broken_version = $info->broken_version;

    $bug_report_info->addon_name = $info->addon_name;
    $bug_report_info->addon_author = $info->addon_author;

    return BugReportRedirector::GetURLForInfo($request, $bug_report_info);
  }

  // Parse description into individual fields, such as OS, GPU.
  public function ParseDescription(string $description): ManiphestReportInfo {
    $info = new ManiphestReportInfo();

    foreach(SplitLines($description) as $line) {
      // Operating system.
      if (StartsWith($line, ManiphestSubmitRedirector::OS_PREFIX)) {
        $info->os = RemovePrefix($line, ManiphestSubmitRedirector::OS_PREFIX);
      }
      // Graphics card.
      if (StartsWith($line, ManiphestSubmitRedirector::GPU_PREFIX)) {
        $info->gpu = RemovePrefix($line, ManiphestSubmitRedirector::GPU_PREFIX);
      }
      // Broken.
      if (StartsWith($line, ManiphestSubmitRedirector::BROKEN_PREFIX)) {
        $info->broken_version =
            RemovePrefix($line, ManiphestSubmitRedirector::BROKEN_PREFIX);
        $info->broken_version =
            RemovePrefix($info->broken_version, "version: ");
      }

      // Add-on name.
      if (StartsWith($line, ManiphestSubmitRedirector::ADDON_NAME_PREFIX)) {
        $info->addon_name =
            RemovePrefix($line, ManiphestSubmitRedirector::ADDON_NAME_PREFIX);
      }
      // Add-on author.
      if (StartsWith($line, ManiphestSubmitRedirector::ADDON_AUTHOR_PREFIX)) {
        $info->addon_author =
            RemovePrefix($line, ManiphestSubmitRedirector::ADDON_AUTHOR_PREFIX);
      }
    }

    return $info;
  }
};

Redirector::Register(new ManiphestSubmitRedirector());

?>
