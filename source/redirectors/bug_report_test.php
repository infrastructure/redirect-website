<?php
// SPDX-FileCopyrightText: 2024 Blender Foundation
//
// SPDX-License-Identifier: MIT-0

////////////////////////////////////////////////////////////////////////////////
// Test suit setup.

BEGIN_TEST_SUITE('BugReportRedirector');

$redirector = new BugReportRedirector();

////////////////////////////////////////////////////////////////////////////////
// Poll() tests.

// Bug submission for for Blender.
EXPECT_TRUE($redirector->Poll(Request::FromArray(
  array('__path__' => '/', 'type' => 'bug_report'))));

// Request which the redirector should not be handling.
EXPECT_FALSE($redirector->Poll(Request::FromArray(
  array('__path__' => '/', 'type' => 'unknown_report'))));

////////////////////////////////////////////////////////////////////////////////
// GetRedirectURL() tests.

// Bug report to the Blender repository.
EXPECT_EQ(GetRediectedUrlForRequest(Request::FromArray(array(
    '__path__' => '/',
    'type' => 'bug_report',
    'project' => 'blender',
    'os' => 'macOS-14.4.1-arm64-arm-64bit 64 Bits',
    'gpu' => 'Apple M2 Ultra Apple 4.1 Metal - 88',
    'broken_version' => 'version: 3.4.1, branch: blender-v3.4-release, commit date: 2022-12-19 17:00, hash: `rB55485cb379f7`',
  ))),
  'https://projects.blender.org/blender/blender/issues/new?template=.gitea/issue_template/bug.yaml&field:body=%2A%2ASystem+Information%2A%2A%0AOperating+system%3A+macOS-14.4.1-arm64-arm-64bit+64+Bits%0AGraphics+card%3A+Apple+M2+Ultra+Apple+4.1+Metal+-+88%0A%0A%2A%2ABlender+Version%2A%2A%0ABroken%3A+version%3A+version%3A+3.4.1%2C+branch%3A+blender-v3.4-release%2C+commit+date%3A+2022-12-19+17%3A00%2C+hash%3A+%60rB55485cb379f7%60%0AWorked%3A+%28newest+version+of+Blender+that+worked+as+expected%29%0A%0A%2A%2AShort+description+of+error%2A%2A%0A%5BPlease+fill+out+a+short+description+of+the+error+here%5D%0A%0A%2A%2AExact+steps+for+others+to+reproduce+the+error%2A%2A%0A%5BPlease+describe+the+exact+steps+needed+to+reproduce+the+issue%5D%0A%5BBased+on+the+default+startup+or+an+attached+.blend+file+%28as+simple+as+possible%29%5D%0A');

// Bug report to the Blender repository.
EXPECT_EQ(GetRediectedUrlForRequest(Request::FromArray(array(
    '__path__' => '/',
    'type' => 'bug_report',
    'project' => 'blender-addons',
    'os' => 'macOS-14.4.1-arm64-arm-64bit 64 Bits',
    'gpu' => 'Metal API Apple M2 Ultra 1.2',
    'broken_version' => 'version: 4.0.2, branch: blender-v4.0-release, commit date: 2023-12-05 07:41, hash: `9be62e85b727`',
    'addon_name' => '3D Navigation (1, 2, 6)',
    'addon_author' => 'Demohero, uriel',
  ))),
  'https://projects.blender.org/blender/blender-addons/issues/new?template=.gitea/issue_template/bug.yaml&field:body=%2A%2ASystem+Information%2A%2A%0AOperating+system%3A+macOS-14.4.1-arm64-arm-64bit+64+Bits%0AGraphics+card%3A+Metal+API+Apple+M2+Ultra+1.2%0A%0A%2A%2ABlender+Version%2A%2A%0ABroken%3A+version%3A+version%3A+4.0.2%2C+branch%3A+blender-v4.0-release%2C+commit+date%3A+2023-12-05+07%3A41%2C+hash%3A+%609be62e85b727%60%0AWorked%3A+%28newest+version+of+Blender+that+worked+as+expected%29%0A%0A%2A%2AAddon+Information%2A%2A%0AName%3A+3D+Navigation+%281%2C+2%2C+6%29%0AAuthor%3A+Demohero%2C+uriel%0A%0A%2A%2AShort+description+of+error%2A%2A%0A%5BPlease+fill+out+a+short+description+of+the+error+here%5D%0A%0A%2A%2AExact+steps+for+others+to+reproduce+the+error%2A%2A%0A%5BPlease+describe+the+exact+steps+needed+to+reproduce+the+issue%5D%0A%5BBased+on+the+default+startup+or+an+attached+.blend+file+%28as+simple+as+possible%29%5D%0A');

// Test utm_source.
EXPECT_EQ(GetRediectedUrlForRequest(Request::FromArray(array(
    '__path__' => '/',
    'type' => 'bug_report',
    'project' => 'blender',
    'os' => 'macOS-14.4.1-arm64-arm-64bit 64 Bits',
    'gpu' => 'Apple M2 Ultra Apple 4.1 Metal - 88',
    'broken_version' => 'version: 3.4.1, branch: blender-v3.4-release, commit date: 2022-12-19 17:00, hash: `rB55485cb379f7`',
    'utm_source' => 'blender',
  ))),
  'https://projects.blender.org/blender/blender/issues/new?template=.gitea/issue_template/bug.yaml&field:body=%2A%2ASystem+Information%2A%2A%0AOperating+system%3A+macOS-14.4.1-arm64-arm-64bit+64+Bits%0AGraphics+card%3A+Apple+M2+Ultra+Apple+4.1+Metal+-+88%0A%0A%2A%2ABlender+Version%2A%2A%0ABroken%3A+version%3A+version%3A+3.4.1%2C+branch%3A+blender-v3.4-release%2C+commit+date%3A+2022-12-19+17%3A00%2C+hash%3A+%60rB55485cb379f7%60%0AWorked%3A+%28newest+version+of+Blender+that+worked+as+expected%29%0A%0A%2A%2AShort+description+of+error%2A%2A%0A%5BPlease+fill+out+a+short+description+of+the+error+here%5D%0A%0A%2A%2AExact+steps+for+others+to+reproduce+the+error%2A%2A%0A%5BPlease+describe+the+exact+steps+needed+to+reproduce+the+issue%5D%0A%5BBased+on+the+default+startup+or+an+attached+.blend+file+%28as+simple+as+possible%29%5D%0A&utm_source=blender');

////////////////////////////////////////////////////////////////////////////////
// Test suit teardown.

END_TEST_SUITE()

?>
