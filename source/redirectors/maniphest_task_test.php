<?php
// SPDX-FileCopyrightText: 2024 Blender Foundation
//
// SPDX-License-Identifier: MIT-0

////////////////////////////////////////////////////////////////////////////////
// Test suit setup.

BEGIN_TEST_SUITE('ManiphestTaskRedirector');

$redirector = new ManiphestTaskRedirector();

////////////////////////////////////////////////////////////////////////////////
// Poll() tests.

EXPECT_TRUE($redirector->Poll(Request::FromArray(
  array('__path__' => '/',
        'type' => 'maniphest_task',
        'id' => '123'))));

// Request which the redirector should not be handling.
EXPECT_FALSE($redirector->Poll(Request::FromArray(
  array('__path__' => '/maniphest/task/edit/form/1'))));

////////////////////////////////////////////////////////////////////////////////
// GetRedirectURL() tests.

// Task in the Blender repository.
EXPECT_EQ(GetRediectedUrlForRequest(Request::FromArray(array(
    '__path__' => '/',
    'type' => 'maniphest_task',
    'id' => 164,
  ))),
'https://projects.blender.org/blender/blender/issues/164');

// Task in the Addons repository.
EXPECT_EQ(GetRediectedUrlForRequest(Request::FromArray(array(
    '__path__' => '/',
    'type' => 'maniphest_task',
    'id' => 2329,
  ))),
'https://projects.blender.org/blender/blender-addons/issues/2329');

// Redirect to an unknown task.
EXPECT_EQ(GetRediectedUrlForRequest(Request::FromArray(array(
    '__path__' => '/',
    'type' => 'maniphest_task',
    'id' => 123,
  ))),
'https://projects.blender.org/');

// Test utm_source.
EXPECT_EQ(GetRediectedUrlForRequest(Request::FromArray(array(
    '__path__' => '/',
    'type' => 'maniphest_task',
    'id' => 164,
    'utm_source' => 'blender'
  ))),
'https://projects.blender.org/blender/blender/issues/164?utm_source=blender');

////////////////////////////////////////////////////////////////////////////////
// Test suit teardown.

END_TEST_SUITE()

?>
