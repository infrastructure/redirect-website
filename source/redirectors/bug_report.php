<?php
// SPDX-FileCopyrightText: 2024 Blender Foundation
//
// SPDX-License-Identifier: MIT-0

// Redirector for bug reports.
// Supports both Blender and Blender-Addons projects.

class BugReportInfo {
  const PROJECT_UNKNOWN = '';
  const PROJECT_BLENDER = 'blender';
  const PROJECT_BLENDER_ADDONS = 'blender-addons';

  public $project = '';

  // Operating system at which bug happens.
  public $os = '';

  // GPU used by Blender for display.
  // This is a display GPU.
  public $gpu = '';

  // String representation of Blender version with all possible details (commit
  // hash etc) against which the report is submitted.
  public $broken_version = '';

  // For add-ons: add-on name and author.
  public $addon_name = '';
  public $addon_author = '';
};

class BugReportRedirector extends Redirector {
  // Mapping from project name to Gitea-like org/repo convention.
  private const PROJECT_TO_ORG_REPO = array(
    BugReportInfo::PROJECT_BLENDER => 'blender/blender',
    BugReportInfo::PROJECT_BLENDER_ADDONS => 'blender/blender-addons',
  );

  private const PROJECT_TO_TEMPLATE = array(
    BugReportInfo::PROJECT_BLENDER => '**System Information**
Operating system: %%OS%%
Graphics card: %%GPU%%

**Blender Version**
Broken: version: %%BROKEN_VERSION%%
Worked: (newest version of Blender that worked as expected)

**Short description of error**
[Please fill out a short description of the error here]

**Exact steps for others to reproduce the error**
[Please describe the exact steps needed to reproduce the issue]
[Based on the default startup or an attached .blend file (as simple as possible)]
',

BugReportInfo::PROJECT_BLENDER_ADDONS => '**System Information**
Operating system: %%OS%%
Graphics card: %%GPU%%

**Blender Version**
Broken: version: %%BROKEN_VERSION%%
Worked: (newest version of Blender that worked as expected)

**Addon Information**
Name: %%ADDON_NAME%%
Author: %%ADDON_AUTHOR%%

**Short description of error**
[Please fill out a short description of the error here]

**Exact steps for others to reproduce the error**
[Please describe the exact steps needed to reproduce the issue]
[Based on the default startup or an attached .blend file (as simple as possible)]
',
  );

  public function Poll(Request $request): bool {
    DebugPrint('BugReportRedirector::Poll');
    return $request->Get('__path__') == '/' &&
           $request->Get('type') == 'bug_report';
  }

  public function GetRedirectURL(Request $request): string {
    DebugPrint('BugReportRedirector::GetRedirectURL');

    $info = new BugReportInfo();

    $info->project = $request->Get('project');

    $info->os = $request->Get('os');
    $info->gpu = $request->Get('gpu');
    $info->broken_version = $request->Get('broken_version');

    $info->addon_name = $request->Get('addon_name');
    $info->addon_author = $request->Get('addon_author');

    return BugReportRedirector::GetURLForInfo($request, $info);
  }

  public static function GetURLForInfo(Request $request,
                                       BugReportInfo $info): string {
    DebugPrint('BugReportRedirector::GetURLForInfo');

    if (!array_key_exists($info->project,
                          BugReportRedirector::PROJECT_TO_ORG_REPO)) {
      DebugPrint('Unknown project ' . $info->project);
      return 'https://projects.blender.org/';
    }

    if (!array_key_exists($info->project,
                          BugReportRedirector::PROJECT_TO_TEMPLATE)) {
      DebugPrint('To template for project ' . $info->project);
      return 'https://projects.blender.org/';
    }

    $org_repo = BugReportRedirector::PROJECT_TO_ORG_REPO[$info->project];
    $template = BugReportRedirector::PROJECT_TO_TEMPLATE[$info->project];

    $body = SubstituteTemplate($template, array(
      'OS' => $info->os,
      'GPU' => $info->gpu,
      'BROKEN_VERSION' => $info->broken_version,

      'ADDON_NAME' => $info->addon_name,
      'ADDON_AUTHOR' => $info->addon_author,
    ));

    $encoded_body = urlencode($body);

    $url = "https://projects.blender.org/$org_repo/issues/new?template=.gitea/issue_template/bug.yaml&field:body=$encoded_body";

    return Redirector::FinalizeURL($request, $url);
  }
};

Redirector::Register(new BugReportRedirector());

?>
