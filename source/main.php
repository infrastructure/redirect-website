<?php
// SPDX-FileCopyrightText: 2024 Blender Foundation
//
// SPDX-License-Identifier: MIT-0

error_reporting(E_ALL | E_STRICT);

require_once 'include_all.php';

function main() {
  $request = Request::FromGET();

  $redirector = Redirector::GetReditrctor($request);
  if (!$redirector) {
    DebugPrint('No handler found for the requested redirect.');
    return;
  }

  $redirector->Handle($request);
  DebugPrint('Redirect request handled');
}

?>
