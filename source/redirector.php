<?php
// SPDX-FileCopyrightText: 2024 Blender Foundation
//
// SPDX-License-Identifier: MIT-0

// A base class for all redirectors.
class Redirector {
  private static $all_redirectors_ = array();

  // Register redirector.
  // The redirector is an object of a class derived from Redirector.
  public static function Register(Redirector $redirector) {
    Redirector::$all_redirectors_[] = $redirector;
  }

  // Get all currently registered redirectors.
  public static function GetAllRedirectors(): array {
    return Redirector::$all_redirectors_;
  }

  // Get redirector for the iven request.
  public static function GetReditrctor(Request $request): ?Redirector {
    foreach (Redirector::GetAllRedirectors() as $redirector) {
      if ($redirector->Poll($request)) {
        return $redirector;
      }
    }
    return null;
  }

  // Poll - check whether this redirector can be responsible for handling the
  // given request.
  public function Poll(Request $request): bool {
    throw new Exception('Subclass needs to implement Poll()');
  }

  // Get URL to which redirection needs to happen.
  public function GetRedirectURL(Request $request): string {
    throw new Exception('Subclass needs to implement GetRedirectURL()');
  }

  // Handle the given request.
  // It is spected to only be called if Poll() function returned true for the
  // given request.
  public function Handle(Request $request) {
    $url = $this->GetRedirectURL($request);
    if (!$url) {
      Redirector::FatalError('Unknown error handling URL');
    }
    Redirector::RedirectTo($url);
  }

  protected static function FatalError(string $message) {
    print('FATAL ERROR: ' . htmlspecialchars($message) . "\n");
    die(1);
  }

  protected static function FinalizeURL(Request $request, string $url) {
    $utm_source = $request->Get('utm_source');
    if ($utm_source) {
      $url = AppendQueryParameter($url, 'utm_source', $utm_source);
    }
    return $url;
  }

  protected static function RedirectTo(string $url) {
    global $DEBUG_;
    if ($DEBUG_) {
      DebugPrint("Redirecting to $url");
      return;
    }
      header("Location: $url", true, 307);
  }
};

?>
