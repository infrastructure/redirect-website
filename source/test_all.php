<?php
// SPDX-FileCopyrightText: 2024 Blender Foundation
//
// SPDX-License-Identifier: MIT-0

// Entry point for all tests.
// Usage: php test_all.php

require_once 'include_all.php';

$current_suit_name_ = '';
$current_suit_passed_ = false;

$all_tests_suits_passed_ = true;

function BEGIN_TEST_SUITE(string $name) {
  global $current_suit_passed_;
  global $current_suit_name_;

  if ($current_suit_name_) {
    throw new Exception('Nexted test suits are not supported');
  }

  print("--> Beginning tests for suite '$name'\n");

  $current_suit_passed_ = true;
}

function END_TEST_SUITE() {
  global $current_suit_passed_;
  global $all_tests_suits_passed_;

  if ($current_suit_passed_) {
    print("    PASSED\n");
  } else {
    print("    FAILED\n");
  }

  $all_tests_suits_passed_ &= $current_suit_passed_;
  $current_suit_name_ = '';
}

function AddFailure($message) {
  global $current_suit_passed_;

  print("    Failue: $message\n");
  debug_print_backtrace();
  $current_suit_passed_ = false;
}

function EXPECT_FALSE($value) {
  if ($value) {
    AddFailure("Expected `false` got `true`");
  }
}

function EXPECT_TRUE($value) {
  if (!$value) {
    AddFailure("Expected `true` got `false`");
  }
}

function EXPECT_EQ($a, $b) {
  if ($a != $b) {
    AddFailure("Expected equality of `$a` and `$b`");
  }
}

require_once 'util_test.php';

// Helper function which gest redirector and uses it to resolve the URL.
function GetRediectedUrlForRequest(Request $request) {
  $redirector = Redirector::GetReditrctor($request);
  if (!$redirector) {
    AddFailure("Unknown redirector for request");
    return "";
  }
  return $redirector->GetRedirectURL($request);
}

require_once 'redirectors/bug_report_test.php';
require_once 'redirectors/maniphest_submit_test.php';
require_once 'redirectors/maniphest_task_test.php';

if ($all_tests_suits_passed_) {
  print("==> All tests: PASSED!\n");
} else {
  print("==> Tests: FAILED!\n");
  die(1);
}

?>
