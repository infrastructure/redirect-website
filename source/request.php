<?php
// SPDX-FileCopyrightText: 2024 Blender Foundation
//
// SPDX-License-Identifier: MIT-0

class Request {
  private $parameters_ = array();

  // Construct request from the current state of global GET parameters.
  public static function FromGET(): Request {
    $request = new Request();
    $request->parameters_ = $_GET;
    return $request;
  }

  public static function FromArray(array $array): Request {
    $request = new Request();
    $request->parameters_ = $array;
    return $request;
  }

  public function Get(string $key, string $default = ''): string {
    if (!array_key_exists($key, $this->parameters_)) {
      return $default;
    }
    return $this->parameters_[$key];
  }
};

?>
