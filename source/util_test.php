<?php
// SPDX-FileCopyrightText: 2024 Blender Foundation
//
// SPDX-License-Identifier: MIT-0

////////////////////////////////////////////////////////////////////////////////
// Test suit setup.

BEGIN_TEST_SUITE('Util');

////////////////////////////////////////////////////////////////////////////////
// Tests.

EXPECT_EQ(SubstituteTemplate('', array()), '');
EXPECT_EQ(SubstituteTemplate('Hello, World!', array()), 'Hello, World!');
EXPECT_EQ(SubstituteTemplate(
  'Hello, %%SUBJECT%%!', array('SUBJECT' => 'World')),
  'Hello, World!');

EXPECT_EQ(AppendQueryParameter('/foo', 'id', '123'), '/foo?id=123');
EXPECT_EQ(AppendQueryParameter('/foo?', 'id', '123'), '/foo?&id=123');
EXPECT_EQ(AppendQueryParameter('/foo?a=b', 'id', '123'), '/foo?a=b&id=123');

////////////////////////////////////////////////////////////////////////////////
// Test suit teardown.

END_TEST_SUITE()

?>
