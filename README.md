Blender Redirector
==================

**Blender Redirector** is a user-facing website which takes care of redirecting
certain semantic entities to their final destination, allowing to hide specifics
of the current configuration.

For example, it is possible to request the redirector to ask "please direct me
to a Blender bug submission form", and the redirtector will take care of figuring
out which final site the request should land on.

The practical issue this redirector solves is backward compatibility of older
systems, so that changes to the Blender infrastructure does not break old
assumptions.

Supported Redirects
===================

All redirects supports an optional `utm_source` argument which is passed through
via the redirect to the final destination.

* Redirect to a bug report submission:
  `/?type=bug_report&project=<project>&os=<os>&gpu=<gpu>&broken_version=<broken_version>&addon_name=<addon_name>&addon_author=<addon_author>`

  - `project` is one of `blender` or `blender-addons`. Any other value will
    redirect to the home page of the bug tracking system (currently https://projects.blender.org).
  - `os`, `gpu`, `broken_version` are the operating system, GPU, and full Blender
    version (including hash) of the system from which the report is being submitted.
  - `addon_name` and `addon_author` is the name of the addon and name of its
    author when bug report is about an addon.

Legacy redirects
----------------

Redirects which were put to support Blender versions prior to the redictor
website.

* Redirect from Phabricator's maniphest submission form to the new submission
  form: `/?type=maniphest_form?id=<form_id>&description=<description>`

  - `form_id` is an identifier of a form. Values could be 1 for Blender bug,
    and 2 for blender-addons bug. Any other value will redirect to the home
    page of the bug tracking system (currently https://projects.blender.org).

  - `description` is a plain text description of the bug report, with some
    template fields pre-filled.

* Redirect from Phabricator's maniphest task it to the new system:
  `/?type=maniphest_task?id=<id>`

  - `id` is an identifier of the task ID. The redirector checks which project
    the report used to belong to, and does redirect to the corresponding issue
    in that project.

    If the given `id` is unknown then redirects happens to the home page of the
    bug tracking system (currently https://projects.blender.org).

Apache configuration
--------------------

A simple example configuration to let local development up-and-running, and to
simplify deployment to the final production. The assumption is that the
repository is checked out to the `/var/www/`.

Configuration for the `redirect.blender.org`:

```
<VirtualHost *:80>
  Define PROJECT_DOCUMENT_ROOT /var/www/redirect-website/

  ServerName redirect.blender.org

  ServerAdmin webmaster@localhost
  DocumentRoot ${PROJECT_DOCUMENT_ROOT}/webroot

  <Directory />
    DirectoryIndex index.php
    Require all granted
  </Directory>

  RewriteEngine on
  RewriteRule ^/robots.txt        -                               [L,QSA]
  RewriteRule ^/favicon.ico       -                               [L,QSA]
  RewriteRule ^(.*)(\?(.*))?$     /index.php?__path__=$1&$2       [B,L,QSA]

  ErrorLog ${PROJECT_DOCUMENT_ROOT}/logs/error.log
  CustomLog ${PROJECT_DOCUMENT_ROOT}/logs/access.log combined
</VirtualHost>
```

Partial configuration for `developer.blender.org`:

```
<VirtualHost *:80>
  ServerName developer.blender.org

  RewriteEngine on

  # Maniphest Tasks.
  RewriteRule    ^/T([0-9]+).*$                         https://redirect.blender.org/?type=maniphest_task&id=$1 [NE,L,QSA,R=307]

  # Report Blender or Addon bug.
  RewriteRule    ^/maniphest/task/edit/form/([0-9]+)    https://redirect.blender.org/?type=maniphest_form&id=$1 [NE,L,QSA,R=307]
</VirtualHost>
```

Testing
-------

There is a very simplistic unit testing put in place, which does not require any
extra dependencies. In order to run the test simply invoke `php source/test_all.php`.

Debugging
---------

When an REDIRECT_DEBUG environment variable is set, or a get parameter with the
same name exists the eredirector will enable debug prints to the output, and will
not perform an actual redirection.

For example: `REDIRECT_DEBUG=1 php ./source/tests_all.php`.

License
=======

The Blender Redirector is licensed under the terms of the [MIT No Attribution](LICENSE).
